all: sum_test

sum_test: sum.o main.o
	g++ -o sum_test sum.o main.o

main.o: header/sum.h cpp/main.cpp
	g++ -c -o main.o cpp/main.cpp

sum.o: header/sum.h cpp/sum.cpp
	g++ -c -o sum.o cpp/sum.cpp

clean:
	rm -f sum_test
	rm -f *.o
